import requests
import time
import sqlite3


access_token = 'cf235c47fbbfe80267270a80bb52cab430fb7071abc82e6a3323ac6813ffa0f476112404dab0ae4caa95d'
url_kfu = f'https://api.vk.com/method/groups.getMembers?group_id=kazan_federal_university&v=5.78&access_token={access_token}'
url_itis = f'https://api.vk.com/method/groups.getMembers?group_id=itis_kfu&v=5.78&access_token={access_token}'


def get_users(url):
    users = []
    offset = 0
    r = requests.get(f"{url}&offset={offset}")
    users += r.json()['response']['items']
    offset += 1000
    while (r.json()['response']['count'] > offset):
        r = requests.get(f"{url}&offset={offset}")
        users += r.json()['response']['items']
        offset += 1000
        time.sleep(0.5)
    return users


def save_in_db(data, table_name):
    with sqlite3.connect("db.sqlite") as connection:
        cursor = connection.cursor()
        for vk_id in data:
            cursor.execute(
                'INSERT INTO ' + table_name + ' (vk_id) VALUES (:vk_id)',
                {'vk_id': vk_id}
            )


if __name__ == '__main__':
    users_from_kfu = get_users(url_kfu)
    users_from_itis = get_users(url_itis)
    print(len(set(users_from_kfu))) # 36825  14.06.18
    print(len(set(users_from_itis))) # 2528  14.06.18
    intersection_users = set(users_from_kfu) & set(users_from_itis)
    print(len(intersection_users))

    print(set(users_from_kfu))
    print(set(users_from_itis))
    print(intersection_users)   #19218506 everywhere

    save_in_db(users_from_kfu, 'students_kfu')
    save_in_db(users_from_itis, 'students_itis')
